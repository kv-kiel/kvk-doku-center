# KVK-Doku Center

Dies ist als zentrale Web-Seite gedacht, die einen gemeinsamen Einstiegspunkt für alle Dokumentationen der [Kanu-Vereinigung-Kiel]( http://www.kv-kiel.de)
bereitstellt, die im Internet verfügbar sind.

Zur Erstellung dieser Seite wurde [Antora](https://antora.org/) benutzt.

Das Ergebnis steht unter https://kv-kiel.gitlab.io/kvk-doku-center/ zur Verfügung.